import React, {Component} from 'react';
import Item from './Item';
import Items from '../mockdata/Items';
import ItemEdit from '../components/ItemEdit';


class ListItem extends Component {
    constructor(props) {    
        super(props);
        // this.handleChange = this.handleChange.bind(this);
        // this.handleShowAlert = this.handleShowAlert.bind(this);
        let arrayLevel = [];
        if(Items.length > 0) {
            for(let i = 0; i < Items.length; i++) {
                if(arrayLevel.indexOf(Items[i].level) === -1) {
                    arrayLevel.push(Items[i].level);
                }
            }
        }
        arrayLevel.sort(function(a, b){return a - b});
        this.state = {
            // items: Items,
            items: this.props.items,
            //part 4 -- edit product
            indexEdit: 0,
            idEdit: '',
            nameEdit: '',
            levelEdit: 0,
            arrayLevel: arrayLevel            
        }
    }

    // handleChange(e) {
    //     this.props.items(e.target.value);
    //   }

    render() {
        return(
            <div className="panel panel-success">
                <div className="panel-heading">List Item</div>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th style={{ width: '10%' }} className="text-center">#</th>
                            <th>Name</th>
                            <th style={{ width: '15%' }} className="text-center">Level</th>
                            <th style={{ width: '15%' }}>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderItem()}
                    </tbody>
                </table>
            </div>
        )
    }

    renderItem = () => {
        const {idEdit,indexEdit,nameEdit,levelEdit,arrayLevel} = this.state;
        let items =this.props.items;
        if(items.length === 0) {
            return <Item item={0} />
        }
        return items.map((item, index) => {
            // console.log("listitem: " + item.id + " " + item.level);
            if(item.id === idEdit) {
                return (
                    <ItemEdit 
                        key={index}
                        indexEdit={indexEdit}
                        nameEdit={nameEdit}
                        levelEdit={levelEdit}
                        arrayLevel={arrayLevel}
                        handleEditClickCancel={this.handleEditClickCancel}
                        handleEditInputChange={this.handleEditInputChange}
                        handleEditSelectChange={this.handleEditSelectChange}    
                        handleEditClickSubmit={this.handleEditClickSubmit}
                
                    />
                )
            }
            return (
                <Item
                    key={index}
                    item={item}
                    index={index}
                    handleShowAlert={this.handleShowAlert}       
                    handleEditItem={this.handleEditItem}             
                  />
            )
        });
    }

    handleShowAlert = (item) => {
        this.props.handleShowAlert(item)
        // console.log(this.props.items);
    }

    handleEditItem = (index,item) => {
        this.setState({
            indexEdit: index,
            idEdit: item.id,
            nameEdit: item.name,
            levelEdit: item.level
        });
    }

    handleEditClickCancel = () => {
        this.setState({
            idEdit: ''
        });
    }

    handleEditInputChange = (value) => {
        this.setState({
            nameEdit: value
        });
    }

    handleEditSelectChange = (value) => {
        this.setState({
            levelEdit: value
        });
    }

    handleEditClickSubmit = () => {
        let {items,idEdit,nameEdit,levelEdit} = this.state;
        if(items.length > 0) { 
            for(let i = 0; i < items.length; i++) {
                if(items[i].id === idEdit) {
                    items[i].name = nameEdit;
                    items[i].level = +levelEdit;
                    break;
                }
            }
        }
        this.setState({
            idEdit: ''
        });
    }
}
      export default ListItem;