import React, { Component } from 'react';
import Title from './components/Title';
import Search from './components/Search';
import Sort from './components/Sort';
import Form from './components/Form';
import ListItem from './components/ListItem';
import SweetAlert from 'sweetalert-react';
import Items from './mockdata/Items';
import './../node_modules/sweetalert/dist/sweetalert.css';
import { v4 as uuidv4 } from 'uuid';
import { orderBy as orderByld } from 'lodash';

class App extends Component {    
    constructor(props) {    
        super(props);  
        this.handleSort = this.handleSort.bind(this)
        let arrayLevel = [];
        if(Items.length > 0) {
            for(let i = 0; i < Items.length; i++) {
                if(arrayLevel.indexOf(Items[i].level) === -1) {
                    arrayLevel.push(Items[i].level);
                }
            }
        }
        arrayLevel.sort(function(a, b){return a - b});
        this.state = {    
            items: Items,    
            //show popup and delete product
            showAlert: false,
            titleAlert: '',
            //part 4 -- edit product
            indexEdit: 0,
            idEdit: '',
            nameEdit: '',
            levelEdit: 0,
            showForm: false,
            arrayLevel,
            valueItem: '',
            levelItem: 0,
            sortType: '',
            sortOrder: ''
        }
    }

    render() {
        return (
            <div className="container">
                {/* <button onClick={()=>this.setState({ showAlert: true })}>Alert</button> */}
                <SweetAlert
                    show={this.state.showAlert}
                    title="Delete Item"
                    text={this.state.titleAlert}
                    showCancelButton
                    onOutsideClick={()  => this.setState({ showAlert: false })}
                    onEscapeKey={()     => this.setState({ showAlert: false })}
                    onCancel={()        => this.setState({ showAlert: false })}
                    onConfirm={()       => this.handleDeleteItem()}
                />
                <Title />
                <div className="row">
                    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <Search />
                    </div>
                    <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <Sort sortType={this.state.sortType}
                            sortOrder={this.state.sortOrder}
                            handleSort={this.handleSort}
                        />
                    </div>
                    <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                        <button type="button" className="btn btn-info btn-block marginB10" onClick={this.handleShowForm}> { (this.state.showForm) ? 'Close Item' : 'Add Item' }</button>
                    </div>
                </div>
                <div className="row marginB10">
                    <div className="col-md-offset-7 col-md-5">
                        <Form showForm={this.state.showForm}                            
                            arrayLevel={this.state.arrayLevel}
                            handleFormInputChange={this.handleFormInputChange}
                            levelItem={this.state.levelItem}
                            handleFormSelectChange = {this.handleFormSelectChange}
                            handleFormClickCancel={this.handleFormClickCancel}
                            handleFormClickSubmit={this.handleFormClickSubmit}
                            handleSort={this.handleSort}
                        />
                    </div>
                </div>
                <ListItem handleShowAlert={this.handleShowAlert}
                        items = {this.state.items}
                        handleEditItem={this.handleEditItem}
                        valueItem={this.state.valueItem}
                />
            </div>
        );
    }

     handleShowAlert = (item) => {
        this.setState({
            showAlert: true,
            titleAlert: item.name,
            idAlert: item.id
        });
    }

    handleDeleteItem = () => {
        let {idAlert, items} = this.state;
        if(items.length > 0) {
            for(let i = 0; i < items.length; i++) {
                if(items[i].id === idAlert) {
                    items.splice(i, 1);
                    break;
                }
            }
        }
        this.setState({
            showAlert: false
        });
    }

    handleEditItem = (index,item) => {
        this.setState({
            indexEdit: index,
            idEdit: item.id,
            nameEdit: item.name,
            levelEdit: item.level
        });
    }

    handleEditClickCancel = () => {
        this.setState({
            idEdit: ''
        });
    }

    handleShowForm = () => {
        this.setState({
            showForm: !this.state.showForm
        });
    }

    handleFormInputChange = (value) => {
        this.setState({
            valueItem: value
        });
    }

    handleFormSelectChange = (value) => {
        this.setState({
            levelItem: value
        });
    }

    handleFormClickCancel = () => {
        this.setState({
            valueItem: '',
            levelItem: 0
        });
    }

    handleFormClickSubmit = () => {
        let {valueItem,levelItem} = this.state;
        if(valueItem.trim() === 0) return false;
        let newItem = {
            id: uuidv4(),
            name: valueItem,
            level: +levelItem
        }; 
        Items.push(newItem);
        this.setState({
            items: Items,
            valueItem: '',
            levelItem: 0,
            showForm: false
        });
    }

    handleSort = (sortType,sortOrder) => {
        this.setState({
            sortType: sortType,
            sortOrder: sortOrder
        });
        let {items} = this.state;
        this.setState({
            items: orderByld(items, [sortType],[sortOrder])
        });

        // items.map((item, index) => {console.log("app.js " + item.name + " - " + item.level);});
        // console.log(sortType + " - " + sortOrder);
    }
 
}

export default App